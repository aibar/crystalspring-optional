import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.net.URISyntaxException;
import java.nio.file.Path;
import java.util.Map;

public class AppTest extends Assert {
    @Test
    public void test1() throws URISyntaxException, IOException {
        App app = new App();

        Map<String, Integer> map = app.countWords(readResource("test1.txt"));
        assertTrue(map.get("hello") == 1);
        assertTrue(map.get("ideal") == 1);
        assertTrue(map.get("world") == 1);
    }

    @Test
    public void test2() throws URISyntaxException, IOException {
        App app = new App();

        Map<String, Integer> map = app.countWords(readResource("test2.txt"));
        assertTrue(map.get("hello") == 1);
        assertTrue(map.get("ideal") == 1);
        assertTrue(map.get("world") == 1);
    }

    @Test
    public void test3() throws URISyntaxException, IOException {
        App app = new App();

        Map<String, Integer> map = app.countWords(readResource("test3.txt"));
        assertTrue(map.get("hello") == 1);
        assertTrue(map.get("ideal") == 1);
        assertTrue(map.get("world") == 1);
    }

    @Test
    public void test4() throws URISyntaxException, IOException {
        App app = new App();

        Map<String, Integer> map = app.countWords(readResource("test4.txt"));
        assertTrue(map.get("hello") == 1);
        assertTrue(map.get("ideal") == 1);
        assertTrue(map.get("world") == 1);
    }

    @Test
    public void test5() throws URISyntaxException, IOException {
        App app = new App();

        Map<String, Integer> map = app.countWords(readResource("test5.txt"));
        assertTrue(map.get("hello") == 2);
        assertTrue(map.get("ideal") == 2);
        assertTrue(map.get("world") == 1);
    }

    private String readResource(String resource) {
        java.net.URL url = getClass().getResource(resource);
        try {
            Path resPath = java.nio.file.Paths.get(url.toURI());
            return new String(java.nio.file.Files.readAllBytes(resPath), "UTF8");
        } catch (URISyntaxException | IOException e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }
}
