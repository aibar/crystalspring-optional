import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class App {
    public Map<String, Integer> countWords(String text) {
        try {
            List<String> words = readWords(text);
            Map<String, Integer> result = new HashMap<>();
            for (String word : words) {
                if (!result.containsKey(word)) {
                    result.put(word, 1);
                } else {
                    result.put(word, result.get(word) + 1);
                }
            }
            return result;
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private List<String> readWords(String text) {
        List<String> result = new ArrayList<>();
        String word = "";
        int i = 0;
        while (i < text.length()) {
            if (text.charAt(i) == ' ') {
                while (i < text.length() && text.charAt(i) == ' ') i++;
                result.add(word);
                word = "";
            } else {
                word += text.charAt(i);
                i++;
            }
        }
        result.add(word);
        return result;
    }
}
